# Keycloak Install

Scripts de instalação e configuração do Keycloak (latest)

`keycloak-install.sh` script para instalação

`keycloak.cli` script JBOSS/Wildfly para configuração

`keycloak-install.sql` sql para criação da base de dados (sem tabelas)

## Pré requistos para o host keycloak

- Ubuntu 18.04
- MySQL 8 (preferencialmente um host exclusivo)
    - database keycloak
    - usuário keycloak
- JDK >= 8

> O script de instalação instala o Amazon Corretto 8 JDK, mas, pode ser outro. Neste caso, tem que instalar manualmente. <br>
> A versão do keycloak é fixada no script para evitar problema de incompatibilidade no cluster. <br>
> O keycloak já vem com o JBOSS/Wildfly 13 junto. 


## Procedimento de instalação

1. Criar o usuário e a base de dados no MySQL
2. Fazer o download do script `keycloak-install.sh`
3. Editar pelo menos as variárveis de ambiente abaixo:
   - **WILDFLY_BIND** com o IP do host
   - **WILDFLY_CLUSTER** com uma lista de IPs do cluster inicial. Formato: x.x.x.x[7600],y.y.y.y[7600]
   - **DB_ADDR** com o IP do banco de dados
   - **DB_PASSWORD** com a senha do usuario keycloak
4. Executar o script como root:`bash keycloak-install.sh`

  
