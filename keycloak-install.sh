#!/bin/bash -e

KEYCLOAK_VERSION=12.0.1
JDBC_MYSQL_VERSION=8.0.22
LANG=en_US.UTF-8

WILDFLY_CONFIG=standalone-ha.xml
WILDFLY_MODE=standalone
WILDFLY_BIND=0.0.0.0
WILDFLY_HOME=/opt/keycloak
WILDFLY_CLUSTER=
#WILDFLY_CLUSTER=host1[7600],host2[7600]

DB_ADDR=127.0.0.1
DB_PORT=3306
DB_DATABASE=keycloak
DB_USER=keycloak
DB_PASSWORD=keycloak
JDBC_PARAMS=?characterEncoding=UTF-8


KEYCLOAK_DIST=https://github.com/keycloak/keycloak/releases/download/$KEYCLOAK_VERSION/keycloak-$KEYCLOAK_VERSION.tar.gz

####################
# PRE-REQ JDK >= 8 #
####################

DEBIAN_FRONTEND=noninteractive apt-get install -yqq java-common curl pv  

if [[ -n $(type -p java) ]]; then
    echo "JDK OK"
else
    echo "Installing AWS CORRETTO JDK 8"
    curl -Ls https://corretto.aws/downloads/latest/amazon-corretto-8-x64-linux-jdk.deb | pv > amazon-corretto-8-x64-linux-jdk.deb
    DEBIAN_FRONTEND=noninteractive apt-get install -yqq ./amazon-corretto-8-x64-linux-jdk.deb
    rm -f ./amazon-corretto-8-x64-linux-jdk.deb
fi


#####################
# Download Keycloak #
#####################

echo "Keycloak from [download]: $KEYCLOAK_DIST"

cd /opt/
curl -Ls $KEYCLOAK_DIST | pv | tar zx
mv /opt/keycloak-* /opt/keycloak

#####################
# Create DB modules #
#####################

echo "Create MYSQL DB modules"

mkdir -p /opt/keycloak/modules/system/layers/base/com/mysql/jdbc/main
cd /opt/keycloak/modules/system/layers/base/com/mysql/jdbc/main
curl -Ls https://repo1.maven.org/maven2/mysql/mysql-connector-java/$JDBC_MYSQL_VERSION/mysql-connector-java-$JDBC_MYSQL_VERSION.jar | pv > mysql-connector-java-$JDBC_MYSQL_VERSION.jar

cat > module.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<module xmlns="urn:jboss:module:1.0" name="com.mysql.jdbc">
  <resources>
    <resource-root path="mysql-connector-java-${JDBC_MYSQL_VERSION}.jar"/>
  </resources>
  <dependencies>
    <module name="javax.api"/>
    <module name="javax.transaction.api"/>
  </dependencies>
</module>
EOF

######################
# Configure Keycloak #
######################
echo "Configure Keycloak"

cd /opt/keycloak

# download do jboss script
curl -LOs https://gitlab.uspdigital.usp.br/stenio/keycloak-install/-/raw/master/keycloak.cli
bin/jboss-cli.sh --file=keycloak.cli
rm -rf /opt/keycloak/standalone/configuration/standalone_xml_history


############################
# Startup Keycloak Scripts #
############################

echo "Startup Keycloak Scripts"

mkdir /etc/keycloak

cat > /etc/keycloak/keycloak.conf <<EOF
WILDFLY_CONFIG=${WILDFLY_CONFIG}
WILDFLY_MODE=${WILDFLY_MODE}
WILDFLY_BIND=${WILDFLY_BIND}
WILDFLY_HOME=${WILDFLY_HOME}
WILDFLY_CLUSTER=${WILDFLY_CLUSTER}

DB_ADDR=${DB_ADDR}
DB_PORT=${DB_PORT}
DB_DATABASE=${DB_DATABASE}
DB_USER=${DB_USER}
DB_PASSWORD=${DB_PASSWORD}
JDBC_PARAMS=${JDBC_PARAMS}
JAVA_OPTS="-Djava.security.egd=file:/dev/urandom -Dinitial.hosts=\${WILDFLY_CLUSTER}"

EOF

cat > /opt/keycloak/bin/launch.sh <<EOF
#!/bin/bash

if [[ "\$1" == "domain" ]]; then
    \${WILDFLY_HOME}/bin/domain.sh -c \$2 -b \$3
else
    \${WILDFLY_HOME}/bin/standalone.sh -c \$2 -b \$3
fi

EOF


cat > /etc/systemd/system/keycloak.service <<EOF
[Unit]
Description=Keycloak Server
After=syslog.target network.target
Before=httpd.service

[Service]
Environment=LAUNCH_JBOSS_IN_BACKGROUND=1
EnvironmentFile=-/etc/keycloak/keycloak.conf
User=keycloak
Group=keycloak
LimitNOFILE=102642
PIDFile=/var/run/keycloak/keycloak.pid
ExecStart=/opt/keycloak/bin/launch.sh \${WILDFLY_MODE} \${WILDFLY_CONFIG} \${WILDFLY_BIND}
StandardOutput=null

[Install]
WantedBy=multi-user.target
EOF



###########
# Garbage #
###########

rm -rf /opt/keycloak/standalone/tmp/auth
rm -rf /opt/keycloak/domain/tmp/auth

###################
# Set permissions #
###################

if id -u "keycloak"; then
echo "User keycloak exists"
else
useradd -r -U -d /opt/keycloak -s /usr/sbin/nologin keycloak
echo "User keycloak created"
fi

chown -RL keycloak: /opt/keycloak
chmod -R g+rwX /opt/keycloak
chmod -R +x /opt/keycloak/bin/

#### 
systemctl daemon-reload

echo "systemctl start keycloak"
